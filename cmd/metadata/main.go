/*
 * Iris Cloud
 *
 * Copyright (c) 2018 GrayMeta
 */

package main

import (
	"flag"
	"log"
	"os"

	"bitbucket.org/iris-cloud-metadata-extractor/extractor/metadata"
	"bitbucket.org/iris-cloud-metadata-extractor/uploader"
)

func main() {

	mediaFilePath := flag.String("file", "", "the media file to be processed")
	userId := flag.String("user", "", "the user id")
	mediaId := flag.String("media", "", "the media id")
	flag.Parse()

	if *mediaFilePath == "" || *userId == "" || *mediaId == "" {
		flag.Usage()
		os.Exit(1)
	}

	data, err := metadata.Extract(*mediaFilePath)
	if err != nil {
		log.Panicf("Error in extracting data: %v", err)
	}

	metadataFile, err := metadata.WriteToJsonFile(&data)
	if err != nil {
		log.Panicf("Error in creating metadata file: %v", err)
	}

	err = uploader.UploadToS3(metadataFile, *userId, *mediaId)
	if err != nil {
		log.Panicf("Error in uploading metadata file to S3: %v", err)
	}

	err = metadata.DeleteTempFile(metadataFile)
	if err != nil {
		log.Printf("Error in deleting temp file: %v", err)
	}
}
