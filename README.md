# Iris Cloud Extractor

* Iris Cloud Extractor
* Golang Implementation
    * Metadata extractor
        * Extract needed metadata from give media file
        * Upload JSON metadata file to AWS S3

## Requirements
* Go
    * https://golang.org/dl/
    * https://golang.org/doc/install
    * Go libraries
        * aws-sdk-go
            * AWS SDK for Go
* Git

## Build Instructions
* Install needed Golang libraries
    * `$ go get github.com/aws/aws-sdk-go`

* Update needed configurations in
    * `config.json`

* Compile
    * `$ go install ./...`
    * This will compile the project and create the binary files
    
* Run
    * Binary files are created under `$GOPATH/bin` directory
    * Metadata extractor
        * `$ ./metadata <required parameters>`
        * For help
            * `$ ./metadata --help`