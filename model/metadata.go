/*
 * Iris Cloud
 *
 * Copyright (c) 2018 GrayMeta
 */

package model

type Metadata struct {
	GeneralMetadata  GeneralMetadata    `json:"general,omitempty"`
	LoudnessMetadata LoudnessMetadata   `json:"loudness,omitempty"`
	VideoMetadata    []VideoMetadata    `json:"videos,omitempty"`
	AudioMetadata    []AudioMetadata    `json:"audios,omitempty"`
	SubtitleMetadata []SubtitleMetadata `json:"captions,omitempty"`
	TimeCodeMetadata []TimeCodeMetadata `json:"timeCodes,omitempty"`
}

type GeneralMetadata struct {
	VideoTrackCount    int `json:"videoTrackCount"`
	AudioTrackCount    int `json:"audioTrackCount"`
	SubtitleTrackCount int `json:"subtitleTrackCount"`
	TimeCodeTrackCount int `json:"timeCodeTrackCount"`
}

type LoudnessMetadata struct {
	LoudnessIntegrated   float32 `json:"integrated,omitempty"`
	LoudnessRange        float32 `json:"range,omitempty"`
	LoudnessMomentaryMax float32 `json:"momentaryMax,omitempty"`
	LoudnessShortTermMax float32 `json:"shortTermMax,omitempty"`
	LoudnessTruePeak     float32 `json:"truePeak,omitempty"`
	LoudnessSamplePeak   float32 `json:"samplePeak,omitempty"`
}

type VideoMetadata struct {
	FileName                string  `json:"fileName,omitempty"`
	StoredWidth             int     `json:"storedWidth"`
	StoredHeight            int     `json:"storedHeight"`
	DisplayWidth            int     `json:"displayWidth"`
	DisplayHeight           int     `json:"displayHeight"`
	DisplayXOffset          int     `json:"displayXOffset"`
	DisplayYOffset          int     `json:"displayYOffset"`
	DisplayAspectRatio      string  `json:"displayAspectRatio,omitempty"`
	FrameRate               float32 `json:"frameRate"`
	TotalFrames             int64   `json:"totalFrames"`
	TotalTime               string  `json:"totalTime,omitempty"`
	ScanMode                string  `json:"scanMode,omitempty"`
	ScanOrder               string  `json:"scanOrder,omitempty"`
	VideoCodec              string  `json:"videoCodec,omitempty"`
	ColorMatrix             string  `json:"colorMatrix,omitempty"`
	ColorPrimaries          string  `json:"colorPrimaries,omitempty"`
	TransferCharacteristics string  `json:"transferCharacteristics,omitempty"`
	ColorSpace              string  `json:"colorSpace,omitempty"`
	ChromaSampling          string  `json:"chromaSampling,omitempty"`
	ColorBitDepth           int     `json:"colorBitDepth"`
	FrameLayout             string  `json:"frameLayout,omitempty"`
	AverageBitrate          float32 `json:"averageBitrate,omitempty"`
	CompressionMode         string  `json:"compressionMode,omitempty"`
	EssenceProfile          string  `json:"essenceProfile,omitempty"`
	CodingSettings          string  `json:"codingSettings,omitempty"`
	Origin                  int     `json:"origin"`
}

type AudioMetadata struct {
	FileName                string  `json:"fileName,omitempty"`
	AudioChannels           int     `json:"audioChannels"`
	AudioCodec              string  `json:"audioCodec,omitempty"`
	AudioSamplingRate       float32 `json:"audioSamplingRate,omitempty"`
	AudioBitRate            float32 `json:"audioBitRate,omitempty"`
	QuantizationDepth       int     `json:"quantizationDepth"`
	OutputSamplingRate      float32 `json:"outputSamplingRate,omitempty"`
	OutputQuantizationDepth int     `json:"outputQuantizationDepth"`
	TotalFrames             int64   `json:"totalFrames"`
	TotalTime               string  `json:"totalTime,omitempty"`
	ChannelLayout           string  `json:"channelLayout,omitempty"`
	EssenceProfile          string  `json:"essenceProfile,omitempty"`
	Language                string  `json:"language,omitempty"`
	DialNorm                string  `json:"dialNorm,omitempty"`
	Origin                  int     `json:"origin"`
}

type SubtitleMetadata struct {
	FileName      string `json:"fileName,omitempty"`
	SubtitleCodec string `json:"subtitleCodec,omitempty"`
	TotalTime     string `json:"totalTime,omitempty"`
	Language      string `json:"language,omitempty"`
	Origin        int    `json:"origin"`
}

type TimeCodeMetadata struct {
	FileName  string `json:"fileName,omitempty"`
	Type      string `json:"type,omitempty"`
	Start     string `json:"start,omitempty"`
	End       string `json:"end,omitempty"`
	DropFrame string `json:"dropFrame,omitempty"`
	Mode      string `json:"mode,omitempty"`
	Origin    int    `json:"origin"`
}
