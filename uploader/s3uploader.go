/*
 * Iris Cloud
 *
 * Copyright (c) 2018 GrayMeta
 */

package uploader

import (
	"bytes"
	"log"
	"os"

	"bitbucket.org/iris-cloud-metadata-extractor/config"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

const (
	S3MetadataDirName  = "metadata"
	S3MetadataFileName = "metadata.json"
)

var configuration = config.Config{}

func init() {
	configuration.Read()
}

// UploadToS3 uploads metadata json file to AWS S3
func UploadToS3(filePath string, userId string, mediaId string) error {

	// Create a single AWS session (we can re use this if we're uploading many files)
	s, err := session.NewSession(&aws.Config{
		Region:      aws.String(configuration.S3Region),
		Credentials: credentials.NewStaticCredentials(configuration.S3AccessKey, configuration.S3SecretKey, "")})
	if err != nil {
		log.Fatal(err)
		return err
	}

	s3FileKey := userId + "/" + mediaId + "/" + S3MetadataDirName + "/" + S3MetadataFileName

	// Upload
	err = addFileToS3(s, filePath, s3FileKey)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return err
}

func addFileToS3(s *session.Session, filePath string, s3FileKey string) error {

	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	fileInfo, _ := file.Stat()
	var size int64 = fileInfo.Size()
	buffer := make([]byte, size)
	file.Read(buffer)

	_, err = s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket:        aws.String(configuration.S3Bucket),
		Key:           aws.String(s3FileKey),
		ACL:           aws.String("public-read"),
		Body:          bytes.NewReader(buffer),
		ContentLength: aws.Int64(size),
		ContentType:   aws.String("application/json"),
	})

	if err != nil {
		return err
	}

	log.Printf("Metadata file successfully uploaded to S3 : %s", s3FileKey)
	return err
}
