/*
 * Iris Cloud
 *
 * Copyright (c) 2018 GrayMeta
 */

package config

import (
	"encoding/json"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
)

type Config struct {
	S3Region    string
	S3AccessKey string
	S3SecretKey string
	S3Bucket    string
}

func (configuration *Config) Read() {
	_, dirName, _, _ := runtime.Caller(0)
	filePath := path.Join(filepath.Dir(dirName), "../config.json")
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&configuration)
	if err != nil {
		log.Fatal(err)
	}
}
