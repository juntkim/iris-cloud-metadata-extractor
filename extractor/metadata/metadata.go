/*
 * Iris Cloud
 *
 * Copyright (c) 2018 GrayMeta
 */

package metadata

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/iris-cloud-metadata-extractor/model"
)

var ErrFFProbeBinNotFound = errors.New("ffprobe not installed or not available in PATH")
var ErrTimeout = errors.New("process timeout exceeded")
var ErrFFProbeExecution = errors.New("failed executing ffprobe")
var ErrUnknownMedia = errors.New("unknown media")
var ErrDecodingJsonData = errors.New("error in decoding json data")
var ErrMediaNotExist = errors.New("invalid path or media does not exist")
var ErrCreatingFile = errors.New("error creating file: ")
var ErrWritingToFile = errors.New("error writing to file: ")

const (
	FfprobeCommand = "ffprobe"

	StreamTypeVideo    = "video"
	StreamTypeAudio    = "audio"
	StreamTypeSubtitle = "subtitle"
	StreamTypeTimeCode = "timecode"

	TempFilePrefix = "metadata-"
)

// Extract gets needed data from media file using ffprobe command
func Extract(filePath string) (model.Metadata, error) {

	log.Printf("Extracting metadata from: %s", filePath)

	_, err := os.Stat(filePath)
	if err != nil {
		return model.Metadata{}, ErrMediaNotExist
	}

	ffprobeData, err := getFFProbeData(filePath)
	if err != nil {
		return model.Metadata{}, err
	}

	var videoMetadatas []model.VideoMetadata
	var audioMetadatas []model.AudioMetadata
	var subtitleMetadatas []model.SubtitleMetadata
	var timeCodeMetadatas []model.TimeCodeMetadata

	for _, stream := range ffprobeData.Streams {

		switch strings.ToLower(stream.CodecType) {
		case StreamTypeVideo:
			videoMetadata := extractVideoMetadata(stream, ffprobeData.Format)
			videoMetadatas = append(videoMetadatas, videoMetadata)
		case StreamTypeAudio:
			audioMetadata := extractAudioMetadata(stream, ffprobeData.Format)
			audioMetadatas = append(audioMetadatas, audioMetadata)
		case StreamTypeSubtitle:
			subtitleMetadata := extractSubtitleMetadata(stream, ffprobeData.Format)
			subtitleMetadatas = append(subtitleMetadatas, subtitleMetadata)
		case StreamTypeTimeCode:
			timeCodeMetadata := extractTimeCodeMetadata(stream, ffprobeData.Format)
			timeCodeMetadatas = append(timeCodeMetadatas, timeCodeMetadata)
		}

	}

	if err != nil {
		return model.Metadata{}, err
	}

	// TODO: dummy timecode metadata
	timeCodeMetadatas = append(timeCodeMetadatas, dummyTimeCodeMetadata(ffprobeData.Format))

	log.Printf("- video streams: %d", len(videoMetadatas))
	log.Printf("- audio streams: %d", len(audioMetadatas))
	log.Printf("- subtitle streams: %d", len(subtitleMetadatas))
	log.Printf("- timecode streams: %d", len(timeCodeMetadatas))

	var generalMetadata model.GeneralMetadata
	generalMetadata.VideoTrackCount = len(videoMetadatas)
	generalMetadata.AudioTrackCount = len(audioMetadatas)
	generalMetadata.SubtitleTrackCount = len(subtitleMetadatas)
	generalMetadata.TimeCodeTrackCount = len(timeCodeMetadatas)

	loudnessMetadata := extractLoudnessMetadata()

	var metadata model.Metadata
	metadata.GeneralMetadata = generalMetadata
	metadata.LoudnessMetadata = loudnessMetadata
	metadata.VideoMetadata = videoMetadatas
	metadata.AudioMetadata = audioMetadatas
	metadata.SubtitleMetadata = subtitleMetadatas
	metadata.TimeCodeMetadata = timeCodeMetadatas

	return metadata, nil
}

func getFFProbeData(filePath string) (model.FFProbeData, error) {

	cmdArgs := []string{
		"-i", filePath,
		"-v", "quiet",
		"-print_format", "json",
		"-show_error",
		"-show_streams",
		"-show_format",
		"-v", "error",
	}

	cmdName, err := exec.LookPath(FfprobeCommand)
	if err != nil {
		return model.FFProbeData{}, ErrFFProbeBinNotFound
	}

	out, err := exec.Command(cmdName, cmdArgs...).Output()
	if err, ok := err.(*exec.ExitError); ok {
		if err2 := analyzeStderr(err.Stderr); err2 != nil {
			return model.FFProbeData{}, err2
		}
		if len(err.Stderr) > 0 {
			return model.FFProbeData{}, ErrFFProbeExecution
		}
	}
	if err != nil {
		return model.FFProbeData{}, ErrFFProbeExecution
	}

	dec := json.NewDecoder(bytes.NewReader(out))
	var ffprobeData model.FFProbeData
	err = dec.Decode(&ffprobeData)
	if err != nil {
		return model.FFProbeData{}, ErrDecodingJsonData
	}

	return ffprobeData, nil
}

func extractLoudnessMetadata() model.LoudnessMetadata {

	var loudnessMetadata model.LoudnessMetadata
	loudnessMetadata.LoudnessIntegrated = -14.3 //TODO
	loudnessMetadata.LoudnessRange = 16.5       //TODO
	loudnessMetadata.LoudnessMomentaryMax = -7  //TODO
	loudnessMetadata.LoudnessShortTermMax = -7  //TODO
	loudnessMetadata.LoudnessTruePeak = -0.02   //TODO
	loudnessMetadata.LoudnessSamplePeak = -0.02 //TODO

	return loudnessMetadata
}

func extractVideoMetadata(stream *model.Stream, format *model.Format) model.VideoMetadata {

	var videoMetadata model.VideoMetadata
	videoMetadata.FileName = format.Filename
	videoMetadata.StoredWidth = stream.CodedWidth
	videoMetadata.StoredHeight = stream.CodedHeight
	videoMetadata.DisplayWidth = stream.Width
	videoMetadata.DisplayHeight = stream.Height
	videoMetadata.DisplayXOffset = 0 //TODO
	videoMetadata.DisplayYOffset = 0 //TODO
	videoMetadata.DisplayAspectRatio = stream.DisplayAspectRatio
	videoMetadata.FrameRate = getFrameRate(stream.AvgFrameRate)
	videoMetadata.TotalFrames = calculateTotalFrames(stream.AvgFrameRate, format.DurationSeconds)
	// duration
	if len(stream.Duration) > 0 {
		videoMetadata.TotalTime = formatDuration(stream.Duration)
	} else {
		videoMetadata.TotalTime = formatDuration(convertFloat64ToString(format.DurationSeconds))
	}
	videoMetadata.ScanMode = stream.FieldOrder //TODO
	videoMetadata.ScanOrder = stream.FieldOrder
	videoMetadata.VideoCodec = stream.CodecLongName
	videoMetadata.ColorMatrix = stream.ColorSpace
	videoMetadata.ColorPrimaries = stream.ColorPrimaries
	videoMetadata.TransferCharacteristics = stream.ColorTransfer
	videoMetadata.ColorSpace = stream.PixFmt
	videoMetadata.ChromaSampling = stream.ChromaLocation
	videoMetadata.ColorBitDepth = convertStringToInt(stream.BitsPerRawSample)
	videoMetadata.FrameLayout = "Progressive" //TODO
	videoMetadata.AverageBitrate = inMegabit(format.BitRate)
	videoMetadata.CompressionMode = "Lossy" //TODO
	videoMetadata.EssenceProfile = stream.Profile
	videoMetadata.CodingSettings = "CABAC=Yes, Ref4" //TODO
	videoMetadata.Origin = stream.Index              //TODO

	return videoMetadata
}

func extractAudioMetadata(stream *model.Stream, format *model.Format) model.AudioMetadata {

	var audioMetadata model.AudioMetadata
	audioMetadata.FileName = format.Filename
	audioMetadata.AudioChannels = stream.Channels
	audioMetadata.AudioCodec = stream.CodecLongName
	audioMetadata.AudioSamplingRate = inKilobit(stream.SampleRate)
	audioMetadata.AudioBitRate = inKilobit(stream.BitRate)
	audioMetadata.QuantizationDepth = 0 //TODO
	audioMetadata.OutputSamplingRate = inKilobit(stream.SampleRate)
	audioMetadata.OutputQuantizationDepth = 0 //TODO
	audioMetadata.TotalFrames = calculateTotalFrames(stream.AvgFrameRate, format.DurationSeconds)
	// duration
	if len(stream.Duration) > 0 {
		audioMetadata.TotalTime = formatDuration(stream.Duration)
	} else {
		audioMetadata.TotalTime = formatDuration(convertFloat64ToString(format.DurationSeconds))
	}
	audioMetadata.ChannelLayout = stream.ChannelLayout
	audioMetadata.EssenceProfile = stream.Profile
	audioMetadata.Language = stream.Tags.Language
	audioMetadata.DialNorm = "N/A"      //TODO
	audioMetadata.Origin = stream.Index //TODO

	return audioMetadata
}

func extractSubtitleMetadata(stream *model.Stream, format *model.Format) model.SubtitleMetadata {

	var subtitleMetadata model.SubtitleMetadata
	subtitleMetadata.FileName = format.Filename
	subtitleMetadata.SubtitleCodec = stream.CodecLongName
	// duration
	if len(stream.Duration) > 0 {
		subtitleMetadata.TotalTime = formatDuration(stream.Duration)
	} else {
		subtitleMetadata.TotalTime = formatDuration(convertFloat64ToString(format.DurationSeconds))
	}
	subtitleMetadata.Language = stream.Tags.Language
	subtitleMetadata.Origin = stream.Index //TODO

	return subtitleMetadata
}

func extractTimeCodeMetadata(stream *model.Stream, format *model.Format) model.TimeCodeMetadata {

	var timeCodeMetadata model.TimeCodeMetadata
	timeCodeMetadata.FileName = format.Filename
	timeCodeMetadata.Type = "Control Timecode Track (CTC)" //TODO
	timeCodeMetadata.Start = "00:00:00.00"                 //TODO
	// duration
	if len(stream.Duration) > 0 {
		timeCodeMetadata.End = formatDuration(stream.Duration)
	} else {
		timeCodeMetadata.End = formatDuration(convertFloat64ToString(format.DurationSeconds))
	}
	timeCodeMetadata.DropFrame = "Non-drop Frame" //TODO
	timeCodeMetadata.Mode = "Striped"             //TODO
	timeCodeMetadata.Origin = stream.Index        //TODO

	return timeCodeMetadata
}

// TODO: dummy data function for time code metadata (for Sintel video)
func dummyTimeCodeMetadata(format *model.Format) model.TimeCodeMetadata {
	var dummyTimeCode model.TimeCodeMetadata
	dummyTimeCode.FileName = format.Filename
	dummyTimeCode.Type = "Control Timecode Track (CTC)" //TODO
	dummyTimeCode.Start = "00:00:00.000"                //TODO
	dummyTimeCode.End = formatDuration(convertFloat64ToString(format.DurationSeconds))
	dummyTimeCode.DropFrame = "Non-drop Frame" //TODO
	dummyTimeCode.Mode = "Striped"             //TODO
	dummyTimeCode.Origin = 0                   //TODO

	return dummyTimeCode
}

func inMegabit(streamBitRate string) float32 {
	return convertStringToFloat32(streamBitRate) / 1e+6
}

func inKilobit(streamBitRate string) float32 {
	return convertStringToFloat32(streamBitRate) / 1000
}

func getFrameRate(streamFrameRate string) float32 {
	split := strings.Split(streamFrameRate, "/")
	numerator, errN := strconv.ParseFloat(split[0], 32)
	denominator, errD := strconv.ParseFloat(split[1], 32)

	if errN != nil || errD != nil || denominator == 0 {
		return 0
	}

	return float32(numerator / denominator)
}

func calculateTotalFrames(streamFrameRate string, duration float64) int64 {
	return int64(float64(getFrameRate(streamFrameRate)) * duration)
}

func convertStringToInt(stringData string) int {
	result, err := strconv.Atoi(stringData)
	if err != nil {
		return 0
	}
	return result
}

func convertStringToFloat32(stringData string) float32 {
	result, err := strconv.ParseFloat(stringData, 32)
	if err != nil {
		return 0.0
	}
	return float32(result)
}

func convertFloat64ToString(float64Data float64) string {
	return fmt.Sprintf("%f", float64Data)
}

func formatDuration(durationData string) string {
	// add 's' to determine that it's in seconds (10s)
	stringDurationSec := durationData + "s"
	duration, err := time.ParseDuration(stringDurationSec)
	if err != nil {
		log.Printf("invalid duration: %s", stringDurationSec)
		return stringDurationSec
	}

	hour := duration / time.Hour
	duration -= hour * time.Hour
	minute := duration / time.Minute
	duration -= minute * time.Minute
	second := duration / time.Second
	duration -= second * time.Second
	millis := duration / time.Millisecond

	return fmt.Sprintf("%02d:%02d:%02d.%d", hour, minute, second, millis)
}

// WriteToJsonFile creates temporary metadata JSON file
func WriteToJsonFile(metadata *model.Metadata) (string, error) {

	jsonData, err := json.Marshal(metadata)
	if err != nil {
		panic(err)
	}

	// sanity check
	//fmt.Println("json data: ", string(jsonData))

	tempMetadataFile, err := ioutil.TempFile(os.TempDir(), TempFilePrefix)
	if err != nil {
		return "", errors.New(ErrCreatingFile.Error() + tempMetadataFile.Name())
	}
	defer tempMetadataFile.Close()

	_, err = tempMetadataFile.Write(jsonData)
	if err != nil {
		return "", errors.New(ErrWritingToFile.Error() + tempMetadataFile.Name())
	}
	tempMetadataFile.Close()

	log.Printf("JSON data written: %s", tempMetadataFile.Name())
	return tempMetadataFile.Name(), nil
}

// DeleteTempFile deletes the temporary metadata JSON file
func DeleteTempFile(filePath string) error {

	log.Printf("Deleting temp file: %s", filePath)
	err := os.Remove(filePath)
	return err
}

func analyzeStderr(data []byte) error {

	if bytes.Contains(data, []byte("Decoder (codec none) not found")) ||
		bytes.Contains(data, []byte("Failed to read frame size: Could not seek to")) ||
		bytes.Contains(data, []byte("Invalid data found when processing input")) {
		return ErrUnknownMedia
	}

	return nil
}
